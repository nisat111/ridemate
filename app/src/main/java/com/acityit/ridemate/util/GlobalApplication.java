package com.acityit.ridemate.util;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.jakewharton.picasso.OkHttp3Downloader;
import com.squareup.picasso.Picasso;

import io.realm.Realm;
import okhttp3.OkHttpClient;

/**
 * Created by IMTIAZ on 7/10/15.
 */
public class GlobalApplication extends Application {
    private static GlobalApplication instance;

    public static GlobalApplication getInstance() {
        return instance;
    }

    public static Context getContext() {
        return instance.getApplicationContext();
    }

    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        MultiDex.install(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Realm.init(this);

        Picasso.Builder builder = new Picasso.Builder(this);
        builder.downloader(new OkHttp3Downloader(new OkHttpClient()));
        Picasso built = builder.build();
        built.setIndicatorsEnabled(false);
        built.setLoggingEnabled(true);
        Picasso.setSingletonInstance(built);
    }
}