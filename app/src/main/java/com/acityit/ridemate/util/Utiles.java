package com.acityit.ridemate.util;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.LocationManager;
import android.provider.Settings;
import android.util.DisplayMetrics;

import com.acityit.ridemate.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by IMTIAZ on 6/4/17.
 */

public class Utiles {
    public static String WTP_END_POINT = "http://audacityit.com/";
    public static String GOOGLE_MAP_END_POINT = "https://maps.googleapis.com/maps/";

    public static String SELECTED_OPTIONS = "selected_option";

    public static String getDate(long milliSeconds, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static String getDate(String old_date) {
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        SimpleDateFormat formatter = new SimpleDateFormat("MMM dd, hh:mm a");
        Calendar calendar = Calendar.getInstance();
        try {
            Date d = f.parse(old_date);
            long milliseconds = d.getTime();
            calendar.setTimeInMillis(milliseconds);
            return formatter.format(calendar.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static Typeface getCustomFont(Context context, String name) {
        return Typeface.createFromAsset(context.getAssets(),
                "fonts/" + name + ".ttf");
    }

    public static DisplayMetrics getDisplayMetrics(Context context) {
        return context.getResources().getDisplayMetrics();
    }

    public static void messageDialog(Context context, String title, String content, int type) {
        new SweetAlertDialog(context, type)
                .setTitleText(title)
                .setContentText(content)
                .show();
    }

    public static void GPSEnableAlert(final Context context, String detailsText) {
        LocationManager lm = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                !lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {

            new SweetAlertDialog(context, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(context.getResources().getString(R.string.location_title))
                    .setContentText(detailsText)
                    .setConfirmText(context.getResources().getString(R.string.allow))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();

                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            context.startActivity(intent);
                        }
                    })
                    .setCancelText(context.getResources().getString(R.string.deny))
                    .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            sweetAlertDialog.dismissWithAnimation();

                        }
                    })
                    .show();
        }
    }

    public static boolean checkGPS(Context context) {
        LocationManager lm = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER) || !lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER))
            return false;

        return true;
    }
}
