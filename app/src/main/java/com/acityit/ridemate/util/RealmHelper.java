package com.acityit.ridemate.util;


import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by progmaatic on 6/29/2015.
 */
public class RealmHelper {
    public static Realm getRealmInstance() {
//        return Realm.getDefaultInstance();
        String db_name = "ridemate";
        RealmConfiguration config = new RealmConfiguration.Builder()
                .name(db_name)
                .build();

        return Realm.getInstance(config);
    }


}
