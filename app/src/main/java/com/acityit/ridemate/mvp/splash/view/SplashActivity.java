package com.acityit.ridemate.mvp.splash.view;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.acityit.anim.ExpectAnim;
import com.acityit.ridemate.R;
import com.acityit.ridemate.activities.BaseActivity;
import com.acityit.ridemate.activities.MainActivity;
import com.acityit.ridemate.mvp.splash.ClientPresenter;
import com.acityit.ridemate.mvp.splash.IClientView;
import com.acityit.ridemate.util.ConnectionDetector;
import com.airbnb.lottie.LottieAnimationView;
import com.airbnb.lottie.LottieComposition;
import com.airbnb.lottie.OnCompositionLoadedListener;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.acityit.anim.core.Expectations.atItsOriginalPosition;
import static com.acityit.anim.core.Expectations.invisible;
import static com.acityit.anim.core.Expectations.leftOfParent;
import static com.acityit.anim.core.Expectations.outOfScreen;
import static com.acityit.anim.core.Expectations.sameCenterVerticalAs;
import static com.acityit.anim.core.Expectations.toRightOf;
import static com.acityit.anim.core.Expectations.topOfParent;
import static com.acityit.anim.core.Expectations.visible;
import static com.acityit.anim.core.Expectations.width;

/**
 * Created by IMTIAZ on 6/4/17.
 */

public class SplashActivity extends BaseActivity implements IClientView {

    private String TAG = "SplashActivity";
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1000;

    private ClientPresenter presenter;

    private ImageView imageView;
    private LinearLayout textHolder;
    private ExpectAnim expectAnim;
    private LottieAnimationView animationView;

    private static final float SCALE_SLIDER_FACTOR = 50f;
    String[] charSet = {"R", "I", "D", "E", "M", "A", "T", "E"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);
        presenter = new ClientPresenter(this);

        setupAnim();
    }

    private void checkAccess() {
        if (!ConnectionDetector.isNetworkPresent(this)) {
            new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                    .setTitleText(getResources().getString(R.string.no_internet_title))
                    .setContentText(getResources().getString(R.string.no_internet))
                    .setConfirmText(getResources().getString(R.string.enable))
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sDialog) {
                            sDialog.dismissWithAnimation();
                            SplashActivity.this.finish();
                        }
                    })
                    .show();
        } else {
            presenter.loadClients();
        }
    }

    public void checkLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                        ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                    },
                    MY_PERMISSIONS_REQUEST_LOCATION);
        } else {
            startActivity();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivity();
                } else {
                    new SweetAlertDialog(SplashActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText(getResources().getString(R.string.permission_denied_title))
                            .setContentText(SplashActivity.this.getResources().getString(R.string.permission_denied))
                            .setCancelText("No, thanks")
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    SplashActivity.this.finish();
                                }
                            })
                            .showCancelButton(true)
                            .setConfirmText("Yes, please")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    checkLocationPermission();
                                }
                            })
                            .show();
                }
                return;
            }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void startActivity() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        }, 3000);
    }

    @Override
    public void onClientLoadSuccess() {
        checkLocationPermission();
    }

    @Override
    public void onClientLoadFailure() {
        checkLocationPermission();
    }

    private void setupAnim() {
        imageView = (ImageView) findViewById(R.id.imageView);
        textHolder = (LinearLayout) findViewById(R.id.textHolder);
        animationView = (LottieAnimationView) findViewById(R.id.animation_view);

        setAnimationTransaction();

        new ExpectAnim()
                .expect(imageView)
                .toBe(
                        atItsOriginalPosition(),
                        visible()
                )
                .toAnimation()
                .setDuration(1500)
                .start();

        loadMobilo(0);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                expectAnim.start();
            }
        }, (charSet.length + 2) * 1000);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                animationView.setVisibility(View.VISIBLE);
                animationView.setScale(30 / SCALE_SLIDER_FACTOR);
                if (!animationView.isAnimating()) {
                    animationView.playAnimation();
                }
                checkAccess();
            }
        }, (charSet.length + 3) * 1000);
    }

    private void loadMobilo(final int position) {
        if (position < charSet.length) {
            final String fileName = "Mobilo/" + charSet[position].toUpperCase() + ".json";
            LottieComposition.Factory.fromAssetFileName(SplashActivity.this, fileName,
                    new OnCompositionLoadedListener() {
                        @Override
                        public void onCompositionLoaded(LottieComposition composition) {
                            addComposition(composition, position);
                        }
                    });
        }
    }

    private void addComposition(LottieComposition composition, int position) {
        final PorterDuffColorFilter colorFilter = new PorterDuffColorFilter(Color.WHITE, PorterDuff.Mode.LIGHTEN);
        LottieAnimationView lottieAnimationView = new LottieAnimationView(this);
        lottieAnimationView.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        ));
        lottieAnimationView.setScale((getDisplayMetrics(SplashActivity.this).widthPixels / 40) / SCALE_SLIDER_FACTOR);
        lottieAnimationView.addColorFilter(colorFilter);
        lottieAnimationView.setComposition(composition);
        lottieAnimationView.playAnimation();

        textHolder.addView(lottieAnimationView);
        final int finalPosition = ++position;
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                loadMobilo(finalPosition);
            }
        }, 500);
    }

    private void setAnimationTransaction() {
        new ExpectAnim()
                .expect(imageView)
                .toBe(
                        outOfScreen(Gravity.BOTTOM),
                        invisible()
                )
                .toAnimation()
                .setNow();
        expectAnim = new ExpectAnim()
                .expect(imageView)
                .toBe(
                        topOfParent().withMarginDp(36),
                        leftOfParent().withMarginDp(16),
                        width(40).toDp().keepRatio()
                )
                .expect(textHolder)
                .toBe(
                        toRightOf(imageView).withMarginDp(16),
                        sameCenterVerticalAs(imageView)
                )
                .toAnimation()
                .setDuration(1500);
    }

    public static DisplayMetrics getDisplayMetrics(Context context) {
        return context.getResources().getDisplayMetrics();
    }
}
