package com.acityit.ridemate.mvp.map.view;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.acityit.ridemate.R;
import com.acityit.ridemate.activities.BaseActivity;
import com.acityit.ridemate.editcard.CardPattern;
import com.acityit.ridemate.editcard.EditCard;
import com.acityit.ridemate.fragments.BikeFragment;
import com.acityit.ridemate.fragments.MicroFragment;
import com.acityit.ridemate.fragments.MiniBusFragment;
import com.acityit.ridemate.fragments.NovaFragment;
import com.acityit.ridemate.fragments.SubFragment;
import com.acityit.ridemate.mvp.map.IAddressView;
import com.acityit.ridemate.mvp.map.PlacePresenter;
import com.acityit.ridemate.util.AnimUtiles;
import com.acityit.ridemate.util.ConnectionDetector;
import com.acityit.ridemate.util.CustomViewPager;
import com.acityit.ridemate.util.TinyDB;
import com.acityit.ridemate.util.Util;
import com.acityit.ridemate.util.Utiles;
import com.afollestad.materialdialogs.MaterialDialog;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import cn.pedant.SweetAlert.SweetAlertDialog;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;

/**
 * Created by IMTIAZ on 6/10/17.
 */

public class MapActivity extends BaseActivity implements GoogleApiClient.ConnectionCallbacks, LocationListener, GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback, GoogleMap.OnMyLocationButtonClickListener, GoogleMap.OnMapLoadedCallback, IAddressView, RoutingListener, View.OnClickListener {

    protected GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    protected Location mLastLocation;
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 1000;

    private PlacePresenter presenter;
    private GoogleMap mGoogleMap;

    Marker selectedMarker;
    private final Handler mHandler = new Handler();
    private List<Marker> markers = new ArrayList<Marker>();

    private static final int REQUEST_CODE_AUTOCOMPLETE = 999;
    private static final int[] COLORS = new int[]{R.color.primary_dark, R.color.primary, R.color.primary_light, R.color.accent, R.color.primary_dark_material_light};
    protected LatLng start;
    protected LatLng end;
    Handler handler = new Handler();
    Random random = new Random();
    Runnable runner = new Runnable() {
        @Override
        public void run() {
            // setHasOptionsMenu(true);
        }
    };
    private List<Polyline> polylines;

    private TextView textSeatChooser, textCardNumber, textEuro;
    private ImageView cardImage, imgArrowHandler;
    private TinyDB td;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location_toolbar);
        setupToolBar();

        checkLocationPermission();

        if (isGooglePlayServicesAvailable()) {
            Utiles.GPSEnableAlert(MapActivity.this, getResources().getString(R.string.location_details));
            buildGoogleApiClient();
        } else {
            new SweetAlertDialog(MapActivity.this)
                    .setTitleText("Play Service Unavailable")
                    .setContentText("Google Play Services is Unavailable in you device")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            finish();
                        }
                    })
                    .show();
        }

        setupMap();
        euroInit();
        tabInit();
        radioInit();
    }

    private void setupToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        presenter = new PlacePresenter(this);
        td = new TinyDB(this);
        td.putString(Utiles.SELECTED_OPTIONS, "Micro");
        polylines = new ArrayList<>();
        handler.postDelayed(runner, random.nextInt(2000));

        findViewById(R.id.btnPlaceSearch).setOnClickListener(this);
        findViewById(R.id.imageViewGPS).setOnClickListener(this);
    }

    private void setupMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void euroInit() {
        imgArrowHandler = (ImageView) findViewById(R.id.imgArrowHandler);
        textSeatChooser = (TextView) findViewById(R.id.textSeatChooser);
        textEuro = (TextView) findViewById(R.id.textEuro);
        LinearLayout cardlayout = (LinearLayout) findViewById(R.id.cardlayout);
        final LinearLayout transportLayout = (LinearLayout) findViewById(R.id.transportLayout);
        textCardNumber = (TextView) findViewById(R.id.textCardNumber);
        cardImage = (ImageView) findViewById(R.id.cardImage);

        imgArrowHandler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (transportLayout.isShown()) {
                    AnimUtiles.collapse(transportLayout);
                    AnimUtiles.ImageViewAnimatedChange(MapActivity.this, imgArrowHandler, R.drawable.arrow_up_2);
                } else {
                    AnimUtiles.expand(transportLayout);
                    AnimUtiles.ImageViewAnimatedChange(MapActivity.this, imgArrowHandler, R.drawable.arrow_down_2);
                }
            }
        });

        textSeatChooser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSeatsSelector();
            }
        });

        textEuro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                euroRange();
            }
        });

        cardlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cardPopUp();
            }
        });

        findViewById(R.id.confirmLayout).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar.make(v, td.getString(Utiles.SELECTED_OPTIONS) + " selected", Snackbar.LENGTH_LONG).show();
            }
        });
    }

    private void euroRange() {
        new MaterialDialog.Builder(this)
                .title(R.string.price_range)
                .content(R.string.price_range_details)
                .inputType(InputType.TYPE_TEXT_FLAG_CAP_WORDS)
                .inputRange(3, 12)
                .positiveText(R.string.submit)
                .input(R.string.price_hint, R.string.price_hint, false, new MaterialDialog.InputCallback() {
                    @Override
                    public void onInput(@NonNull MaterialDialog dialog, CharSequence input) {
                        String text = input.toString();
                        if (text.isEmpty() || !text.contains("-")) {
                            new SweetAlertDialog(MapActivity.this, SweetAlertDialog.ERROR_TYPE)
                                    .setTitleText(getResources().getString(R.string.invalid_euro_title))
                                    .setContentText(getResources().getString(R.string.invalid_euro_details))
                                    .show();
                        } else {
                            String data = input.toString();
                            if (!input.toString().contains("€"))
                                data = "€" + input.toString();
                            textEuro.setText(data);
                        }
                    }
                }).show();
    }

    public void showSeatsSelector() {
        new MaterialDialog.Builder(this)
                .items(R.array.seat_arrays)
                .itemsCallback(new MaterialDialog.ListCallback() {
                    @Override
                    public void onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        textSeatChooser.setText(text.toString());
                    }
                })
                .show();
    }

    private void cardPopUp() {
        LayoutInflater inflater = this.getLayoutInflater();
        final View alertLayout = inflater.inflate(R.layout.card_popup, null);

        final AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(getResources().getString(R.string.card_title));
        alert.setCancelable(false);
        alert.setView(alertLayout);
        final EditCard edit_card = (EditCard) alertLayout.findViewById(R.id.cardNumber);
        edit_card.addTextChangedListener(new TextWatcher() {

            private static final int TOTAL_SYMBOLS = 19; // size of pattern 0000-0000-0000-0000
            private static final int TOTAL_DIGITS = 16; // max numbers of digits in pattern: 0000 x 4
            private static final int DIVIDER_MODULO = 5; // means divider position is every 5th symbol beginning with 1
            private static final int DIVIDER_POSITION = DIVIDER_MODULO - 1; // means divider position is every 4th symbol beginning with 0
            private static final char DIVIDER = '-';

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // noop
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // noop
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!isInputCorrect(s, TOTAL_SYMBOLS, DIVIDER_MODULO, DIVIDER)) {
                    s.replace(0, s.length(), buildCorrecntString(getDigitArray(s, TOTAL_DIGITS), DIVIDER_POSITION, DIVIDER));
                }
            }

            private boolean isInputCorrect(Editable s, int totalSymbols, int dividerModulo, char divider) {
                boolean isCorrect = s.length() <= totalSymbols; // check size of entered string
                for (int i = 0; i < s.length(); i++) { // chech that every element is right
                    if (i > 0 && (i + 1) % dividerModulo == 0) {
                        isCorrect &= divider == s.charAt(i);
                    } else {
                        isCorrect &= Character.isDigit(s.charAt(i));
                    }
                }
                return isCorrect;
            }

            private String buildCorrecntString(char[] digits, int dividerPosition, char divider) {
                final StringBuilder formatted = new StringBuilder();

                for (int i = 0; i < digits.length; i++) {
                    if (digits[i] != 0) {
                        formatted.append(digits[i]);
                        if ((i > 0) && (i < (digits.length - 1)) && (((i + 1) % dividerPosition) == 0)) {
                            formatted.append(divider);
                        }
                    }
                }

                return formatted.toString();
            }

            private char[] getDigitArray(final Editable s, final int size) {
                char[] digits = new char[size];
                int index = 0;
                for (int i = 0; i < s.length() && index < size; i++) {
                    char current = s.charAt(i);
                    if (Character.isDigit(current)) {
                        digits[index] = current;
                        index++;
                    }
                }
                return digits;
            }
        });
        alert.setPositiveButton(getResources().getString(R.string.submit), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (edit_card.getCardNumber().isEmpty() || !edit_card.isValid()) {
                    new SweetAlertDialog(MapActivity.this, SweetAlertDialog.ERROR_TYPE)
                            .setTitleText(getResources().getString(R.string.invalid_card_title))
                            .setContentText(getResources().getString(R.string.invalid_card_details))
                            .show();
                } else {
                    dialog.dismiss();
                    String text = edit_card.getCardNumber();
                    String star = "•••• " + text.substring(text.length() - 4);
                    textCardNumber.setText(star);

                    Picasso.with(MapActivity.this)
                            .load(getImageType(text))
                            .placeholder(R.drawable.cd_card)
                            .error(R.drawable.cd_card)
                            .into(cardImage);
                }
            }
        });

        alert.create().show();
    }

    private int getImageType(String cardType) {
        String s = cardType.replace("-", "").trim();
        if (s.startsWith("4") || s.matches(CardPattern.VISA)) {
            return R.drawable.cd_vi;
        } else if (s.matches(CardPattern.MASTERCARD_SHORTER) || s.matches(CardPattern.MASTERCARD_SHORT) || s.matches(CardPattern.MASTERCARD)) {
            return R.drawable.cd_mc;
        } else if (s.matches(CardPattern.AMERICAN_EXPRESS)) {
            return R.drawable.cd_am;
        } else if (s.matches(CardPattern.DISCOVER_SHORT) || s.matches(CardPattern.DISCOVER)) {
            return R.drawable.cd_ds;
        } else if (s.matches(CardPattern.JCB_SHORT) || s.matches(CardPattern.JCB)) {
            return R.drawable.cd_jcb;
        } else if (s.matches(CardPattern.DINERS_CLUB_SHORT) || s.matches(CardPattern.DINERS_CLUB)) {
            return R.drawable.cd_dc;
        } else {
            return R.drawable.cd_card;
        }
    }

    private CustomViewPager viewPager;

    private void tabInit() {
        viewPager = (CustomViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new MiniBusFragment(), "Mini Van");
        adapter.addFrag(new SubFragment(), "SUB");
        adapter.addFrag(new MicroFragment(), "MICRO");
        adapter.addFrag(new BikeFragment(), "BIKE");
        adapter.addFrag(new NovaFragment(), "NOVA");
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(2);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {

            // return null to display only the icon
            return mFragmentTitleList.get(position);
        }
    }

    private void radioInit() {
        RadioGroup transports = (RadioGroup) findViewById(R.id.transportOptions);
        transports.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.tr_minibus:
                        td.putString(Utiles.SELECTED_OPTIONS, "Mini Bus");
                        viewPager.setCurrentItem(0);
                        break;
                    case R.id.tr_sub:
                        td.putString(Utiles.SELECTED_OPTIONS, "SUB");
                        viewPager.setCurrentItem(1);
                        break;
                    case R.id.tr_micro:
                        td.putString(Utiles.SELECTED_OPTIONS, "Micro");
                        viewPager.setCurrentItem(2);
                        break;
                    case R.id.tr_bike:
                        td.putString(Utiles.SELECTED_OPTIONS, "BIKE");
                        viewPager.setCurrentItem(3);
                        break;
                    case R.id.tr_nova:
                        td.putString(Utiles.SELECTED_OPTIONS, "NOVA");
                        viewPager.setCurrentItem(4);
                        break;
                    default:
                        td.putString(Utiles.SELECTED_OPTIONS, "Micro");
                        viewPager.setCurrentItem(2);
                        break;
                }
            }
        });
    }

    public void hideKeyboard() {
        // Check if no view has focus:
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private boolean isGooglePlayServicesAvailable() {
        final boolean[] isAvailable = {true};
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(MapActivity.this);
        if (status != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(status)) {
                Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, MapActivity.this, 0);
                dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {
                        isAvailable[0] = false;
                    }
                });
                dialog.show();
                hideProgressDialog();
            } else {
                hideProgressDialog();
                Toast.makeText(MapActivity.this, "This device is not supported.", Toast.LENGTH_LONG).show();
                isAvailable[0] = false;
            }
        }

        return isAvailable[0];
    }

    public void checkLocationPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                ) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                            ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mGoogleMap.setMyLocationEnabled(true);
                    }

                } else {
                    // Permission denied, Disable the functionality that depends on this permission.
                    hideProgressDialog();
                    new SweetAlertDialog(MapActivity.this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Permission denied")
                            .setContentText(MapActivity.this.getResources().getString(R.string.permission_denied))
                            .setCancelText("No, thanks")
                            .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    finish();
                                }
                            })
                            .showCancelButton(true)
                            .setConfirmText("Yes, please")
                            .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    sweetAlertDialog.dismissWithAnimation();
                                    checkLocationPermission();
                                }
                            })
                            .show();
                }
                return;
            }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    protected synchronized void buildGoogleApiClient() {
        Log.e("APPS", "buildGoogleApiClient");
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
    }

    private void googleApiClientReconnect() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.reconnect();
        } else {
            if (mGoogleApiClient != null) {
                mGoogleApiClient.connect();
            }
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        googleApiClientReconnect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            stopLocationListener();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void stopLocationListener() {
        hideProgressDialog();
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, MapActivity.this);
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        setupLocationRequest();
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClientReconnect();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e("APPS", "onLocationChanged");
        updateLastLocation(location);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        googleApiClientReconnect();
    }

    private void setupLocationRequest() {
        checkLocationPermission();
        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (location != null) {
            updateLastLocation(location);
        }

        startLocationUpdates();
    }

    protected void startLocationUpdates() {
        // Create the location request
        Log.e("APPS", "startLocationUpdates");
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(5000)
                .setFastestInterval(2000);

        checkLocationPermission();
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
    }

    private void updateLastLocation(Location location) {
        mLastLocation = location;
        start = new LatLng(location.getLatitude(), location.getLongitude());
        presenter.loadAddress(new LatLng(location.getLatitude(), location.getLongitude()), this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.setOnMyLocationButtonClickListener(this);
        mGoogleMap.setOnMapLoadedCallback(this);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);

        checkLocationPermission();
        mGoogleMap.setMyLocationEnabled(true);
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onMapLoaded() {
        hideProgressDialog();
    }

    /**
     * Adds a marker to the map.
     */
    public void addMarkerToMap(LatLng latLng) {
        clearMarkers();
        removeSelectedMarker();
        Marker marker = mGoogleMap.addMarker(new MarkerOptions().position(latLng)
                .title("Address")
                .snippet("Current Location"));
        markers.add(marker);

    }

    private void resetMarkers() {
        for (Marker marker : this.markers) {
            marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }
    }

    public void clearMarkers() {
        if (mGoogleMap != null)
            mGoogleMap.clear();
        if (markers != null)
            markers.clear();
    }

    /**
     * Remove the currently selected marker.
     */
    public void removeSelectedMarker() {
        if (markers != null && selectedMarker != null) {
            this.markers.remove(this.selectedMarker);
            this.selectedMarker.remove();
        }

        highLightMarker(0);
    }

    /**
     * Highlight the marker by index.
     */
    private void highLightMarker(int index) {
        if (markers.size() > 0 && index < markers.size())
            highLightMarker(markers.get(index));
    }

    /**
     * Highlight the marker by marker.
     */
    private void highLightMarker(Marker marker) {

		/*
        for (Marker foundMarker : this.markers) {
			if (!foundMarker.equals(marker)) {
				foundMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
			} else {
				foundMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
				foundMarker.showInfoWindow();
			}
		}
		*/
        marker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        //  marker.showInfoWindow();
        //marker.remove();
        //Utils.bounceMarker(googleMap, marker);

        this.selectedMarker = marker;
    }

    public void navigateToPoint(LatLng latLng, float tilt, float bearing, float zoom, boolean animate) {
        CameraPosition position =
                new CameraPosition.Builder().target(latLng)
                        .zoom(zoom)
                        .bearing(bearing)
                        .tilt(tilt)
                        .build();

        changeCameraPosition(position, animate);

    }

    public void navigateToPoint(LatLng latLng, boolean animate) {
        CameraPosition position = new CameraPosition.Builder().target(latLng).build();
        changeCameraPosition(position, animate);
    }

    private void changeCameraPosition(CameraPosition cameraPosition, boolean animate) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);

        if (animate) {
            mGoogleMap.animateCamera(cameraUpdate);
        } else {
            mGoogleMap.moveCamera(cameraUpdate);
        }

    }

    public float bearingBetweenLatLngs(LatLng begin, LatLng end) {
        Location beginL = convertLatLngToLocation(begin);
        Location endL = convertLatLngToLocation(end);

        return beginL.bearingTo(endL);
    }

    private Location convertLatLngToLocation(LatLng latLng) {
        Location loc = new Location("someLoc");
        loc.setLatitude(latLng.latitude);
        loc.setLongitude(latLng.longitude);
        return loc;
    }


    private void configureMapUI(LatLng latLng, final String name, String address, String reference, String placeId) {
        addMarkerToMap(latLng);
        AnimUtiles.expand(findViewById(R.id.pickUpLayout));
        TextView textCurrentLocation = (TextView) findViewById(R.id.textCurrentLocation);

        CameraUpdate center = CameraUpdateFactory.newLatLng(latLng);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(14);

        mGoogleMap.moveCamera(center);
        mGoogleMap.animateCamera(zoom);

        String title = name;
        String snippet = address;

        if (reference.equals("empty_reference") && !placeId.equals("m1")) {
            Place place = findPlaceById(placeId);
            if (place != null) {
                title = place.getName().toString();
                snippet = place.getAddress().toString();
            }
        }

        textCurrentLocation.setText(title + "\n" + snippet);
    }


    private Place findPlaceById(String id) {
        final Place[] place = {null};
        if (TextUtils.isEmpty(id) || mGoogleApiClient == null || !mGoogleApiClient.isConnected())
            return null;

        Places.GeoDataApi.getPlaceById(mGoogleApiClient, id).setResultCallback(new ResultCallback<PlaceBuffer>() {
            @Override
            public void onResult(PlaceBuffer places) {
                if (places.getStatus().isSuccess()) {
                    place[0] = places.get(0);
                }

                //Release the PlaceBuffer to prevent a memory leak
                places.release();
            }
        });

        return place[0];
    }

    @Override
    public void onAddressLoadedSuccess(LatLng latLng, String name, String address, String reference, String placeId) {
        stopLocationListener();
        configureMapUI(latLng, name, address, reference, placeId);
    }

    @Override
    public void onAddressLoadedFailure(LatLng latLng) {
        if (!ConnectionDetector.isNetworkPresent(MapActivity.this))
            stopLocationListener();

        configureMapUI(latLng, getResources().getString(R.string.current_location), getResources().getString(R.string.current_location_details), "empty_reference", "m1");
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnPlaceSearch:
                openAutocompleteActivity();
                break;
            case R.id.imageViewGPS:
                removeSelectedMarker();
                clearMarkers();
                googleApiClientReconnect();
                break;
        }
    }

    /* Route Transaction */

    private void openAutocompleteActivity() {
        try {
            removeSelectedMarker();
            clearMarkers();
            // The autocomplete activity requires Google Play Services to be available. The intent
            // builder checks this and throws an exception if it is not the case.
            AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
                    .setTypeFilter(Place.TYPE_COUNTRY)
                    .setCountry("BD")
                    .build();
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .setFilter(autocompleteFilter)
                    .build(this);
            startActivityForResult(intent, REQUEST_CODE_AUTOCOMPLETE);
        } catch (GooglePlayServicesRepairableException e) {
            // Indicates that Google Play Services is either not installed or not up to openStatus. Prompt
            // the user to correct the issue.
            GoogleApiAvailability.getInstance().getErrorDialog(this, e.getConnectionStatusCode(),
                    0 /* requestCode */).show();
        } catch (GooglePlayServicesNotAvailableException e) {
            // Indicates that Google Play Services is not available and the problem is not easily
            // resolvable.
            String message = "Google Play Services is not available: " +
                    GoogleApiAvailability.getInstance().getErrorString(e.errorCode);

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_AUTOCOMPLETE) {
            if (resultCode == RESULT_OK) {
                AnimUtiles.expand(findViewById(R.id.pagerLayout));
                final Place place = PlaceAutocomplete.getPlace(this, data);
                Log.e("APPS", place.getAddress().toString());
                end = new LatLng(place.getLatLng().latitude, place.getLatLng().longitude);
                if (Util.Operations.isOnline(this)) {
                    route();
                } else {
                    Toast.makeText(this, "No internet connectivity", Toast.LENGTH_SHORT).show();
                }
            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
                Status status = PlaceAutocomplete.getStatus(this, data);
                new SweetAlertDialog(MapActivity.this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText("Oops...")
                        .setContentText("No result found. Try again!")
                        .show();
            } else if (resultCode == RESULT_CANCELED) {
                // Indicates that the activity closed before a selection was made. For sample if
                // the user pressed the back button.
            }
        }
    }

    public void route() {
        showProgressDialog(getResources().getString(R.string.progressing));

        Routing routing = new Routing.Builder()
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(true)
                .waypoints(start, end)
                .build();
        routing.execute();

        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(14);

        mGoogleMap.moveCamera(center);
        mGoogleMap.animateCamera(zoom);

        hideKeyboard();
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        hideProgressDialog();
        if (e != null) {
            Toast.makeText(this, "Error: " + e.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(this, "Something went wrong, Try again", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int i) {
        hideProgressDialog();
        CameraUpdate center = CameraUpdateFactory.newLatLng(start);
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);

        mGoogleMap.moveCamera(center);


        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();

        for (LatLng pt : route.get(0).getPoints()) {
            MarkerOptions options = new MarkerOptions();
            options.position(pt);

            Marker marker = mGoogleMap.addMarker(options);
            markers.add(marker);
            marker.setVisible(false);
        }

        int inx = 0;
        //In case of more than 5 alternative routes
        int colorIndex = 1 % COLORS.length;

        PolylineOptions polyOptions = new PolylineOptions();
        polyOptions.color(getResources().getColor(COLORS[colorIndex]));
        polyOptions.width(10 + inx * 3);
        polyOptions.addAll(route.get(inx).getPoints());
        Polyline polyline = mGoogleMap.addPolyline(polyOptions);
        polylines.add(polyline);

//        "Route " + (inx + 1) +
        Toast.makeText(getApplicationContext(), "Distance - " + route.get(inx).getDistanceValue() + ": duration - " + route.get(inx).getDurationValue(), Toast.LENGTH_SHORT).show();


        // Start marker
        MarkerOptions options = new MarkerOptions();
        options.position(start);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.start_blue));
        Marker startMarker = mGoogleMap.addMarker(options);


        // End marker
        options = new MarkerOptions();
        options.position(end);
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.end_green));
        Marker endMarker = mGoogleMap.addMarker(options);


        animator.startAnimation(false);
    }

    @Override
    public void onRoutingCancelled() {

    }


    private Animator animator = new Animator();
    int currentPt;

    GoogleMap.CancelableCallback MyCancelableCallback =
            new GoogleMap.CancelableCallback() {

                @Override
                public void onCancel() {
                    System.out.println("onCancelled called");
                }

                @Override
                public void onFinish() {


                    if (++currentPt < markers.size()) {


                        float targetBearing = bearingBetweenLatLngs(mGoogleMap.getCameraPosition().target, markers.get(currentPt).getPosition());

                        LatLng targetLatLng = markers.get(currentPt).getPosition();

                        System.out.println("currentPt  = " + currentPt);
                        System.out.println("size  = " + markers.size());
                        //Create a new CameraPosition
                        CameraPosition cameraPosition =
                                new CameraPosition.Builder()
                                        .target(targetLatLng)
                                        .tilt(currentPt < markers.size() - 1 ? 90 : 0)
                                        .bearing(targetBearing)
                                        .zoom(mGoogleMap.getCameraPosition().zoom)
                                        .build();


                        mGoogleMap.animateCamera(
                                CameraUpdateFactory.newCameraPosition(cameraPosition),
                                3000,
                                MyCancelableCallback);
                        System.out.println("Animate to: " + markers.get(currentPt).getPosition() + "\n" +
                                "Bearing: " + targetBearing);


                    } else {

                    }

                }

            };

    public class Animator implements Runnable {

        private static final int ANIMATE_SPEEED = 1500;
        private static final int ANIMATE_SPEEED_TURN = 1000;
        private static final int BEARING_OFFSET = 20;

        private final Interpolator interpolator = new LinearInterpolator();

        int currentIndex = 0;

        float tilt = 90;
        float zoom = 15.5f;
        boolean upward = true;

        long start = SystemClock.uptimeMillis();

        LatLng endLatLng = null;
        LatLng beginLatLng = null;

        boolean showPolyline = false;

        private Marker trackingMarker;

        public void reset() {
            resetMarkers();
            start = SystemClock.uptimeMillis();
            currentIndex = 0;
            endLatLng = getEndLatLng();
            beginLatLng = getBeginLatLng();

        }

        public void stop() {
            trackingMarker.remove();
            mHandler.removeCallbacks(animator);

        }

        public void initialize(boolean showPolyLine) {
            reset();
            this.showPolyline = showPolyLine;

            highLightMarker(0);

            if (showPolyLine) {
                polyLine = initializePolyLine();
            }

            // We first need to put the camera in the correct position for the first run (we need 2 markers for this).....
            LatLng markerPos = markers.get(0).getPosition();
            LatLng secondPos = markers.get(1).getPosition();

            setupCameraPositionForMovement(markerPos, secondPos);

        }

        private void setupCameraPositionForMovement(LatLng markerPos,
                                                    LatLng secondPos) {

            float bearing = bearingBetweenLatLngs(markerPos, secondPos);


            BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.drawable.car_small);

            trackingMarker = mGoogleMap.addMarker(new MarkerOptions().position(markerPos)
                    .icon(icon)
                    .title("Car")
                    .snippet("Car position"));

            CameraPosition cameraPosition =
                    new CameraPosition.Builder()
                            .target(markerPos)
                            .bearing(bearing + BEARING_OFFSET)
                            .tilt(90)
                            .zoom(mGoogleMap.getCameraPosition().zoom >= 16 ? mGoogleMap.getCameraPosition().zoom : 16)
                            .build();

            mGoogleMap.animateCamera(
                    CameraUpdateFactory.newCameraPosition(cameraPosition),
                    ANIMATE_SPEEED_TURN,
                    new GoogleMap.CancelableCallback() {

                        @Override
                        public void onFinish() {
                            System.out.println("finished camera");
                            animator.reset();
                            Handler handler = new Handler();
                            handler.post(animator);
                        }

                        @Override
                        public void onCancel() {
                            System.out.println("cancelling camera");
                        }
                    }
            );
        }

        private Polyline polyLine;
        private PolylineOptions rectOptions = new PolylineOptions();


        private Polyline initializePolyLine() {
            //polyLinePoints = new ArrayList<LatLng>();
            rectOptions.add(markers.get(0).getPosition());
            return mGoogleMap.addPolyline(rectOptions);
        }

        /**
         * Add the marker to the polyline.
         */
        private void updatePolyLine(LatLng latLng) {
            List<LatLng> points = polyLine.getPoints();
            points.add(latLng);
            polyLine.setPoints(points);
        }


        public void stopAnimation() {
            animator.stop();
        }

        public void startAnimation(boolean showPolyLine) {
            if (markers.size() > 1) {
                animator.initialize(showPolyLine);
            }
        }


        @Override
        public void run() {

            long elapsed = SystemClock.uptimeMillis() - start;
            double t = interpolator.getInterpolation((float) elapsed / ANIMATE_SPEEED);


            double lat = t * endLatLng.latitude + (1 - t) * beginLatLng.latitude;
            double lng = t * endLatLng.longitude + (1 - t) * beginLatLng.longitude;
            LatLng newPosition = new LatLng(lat, lng);

            trackingMarker.setPosition(newPosition);

            if (showPolyline) {
                updatePolyLine(newPosition);
            }


            if (t < 1) {
                mHandler.postDelayed(this, 16);
            } else {

                System.out.println("Move to next marker.... current = " + currentIndex + " and size = " + markers.size());
                // imagine 5 elements -  0|1|2|3|4 currentindex must be smaller than 4
                if (currentIndex < markers.size() - 2) {

                    currentIndex++;

                    endLatLng = getEndLatLng();
                    beginLatLng = getBeginLatLng();


                    start = SystemClock.uptimeMillis();

                    LatLng begin = getBeginLatLng();
                    LatLng end = getEndLatLng();

                    float bearingL = bearingBetweenLatLngs(begin, end);

                    highLightMarker(currentIndex);

                    CameraPosition cameraPosition =
                            new CameraPosition.Builder()
                                    .target(end) // changed this...
                                    .bearing(bearingL + BEARING_OFFSET)
                                    .tilt(tilt)
                                    .zoom(mGoogleMap.getCameraPosition().zoom)
                                    .build();


                    mGoogleMap.animateCamera(
                            CameraUpdateFactory.newCameraPosition(cameraPosition),
                            ANIMATE_SPEEED_TURN,
                            null
                    );

                    start = SystemClock.uptimeMillis();
                    mHandler.postDelayed(animator, 16);

                } else {
                    currentIndex++;
                    highLightMarker(currentIndex);
                    //    stopAnimation();
                }

            }
        }

        private LatLng getEndLatLng() {
            return markers.get(currentIndex + 1).getPosition();
        }

        private LatLng getBeginLatLng() {
            return markers.get(currentIndex).getPosition();
        }

        private void adjustCameraPosition() {

            if (upward) {

                if (tilt < 90) {
                    tilt++;
                    zoom -= 0.01f;
                } else {
                    upward = false;
                }

            } else {
                if (tilt > 0) {
                    tilt--;
                    zoom += 0.01f;
                } else {
                    upward = true;
                }
            }
        }
    }

}
