package com.acityit.ridemate.mvp.map;

import com.acityit.ridemate.models.place_details.Result;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by IMTIAZ on 6/11/17.
 */

public interface IPlaceLoadedFinished {
    void onNetworkSuccess(Result value);
    void onNetworkFailure(LatLng latLng);
}
