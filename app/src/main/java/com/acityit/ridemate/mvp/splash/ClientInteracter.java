package com.acityit.ridemate.mvp.splash;

import com.acityit.ridemate.retrofit.client_model.ClientModel;
import com.acityit.ridemate.retrofit.intefaces.ApiRequestUrls;
import com.acityit.ridemate.util.RealmHelper;
import com.acityit.ridemate.util.Utiles;

import io.realm.Realm;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.acityit.ridemate.retrofit.api.ApiClient.getApiInterface;

/**
 * Created by IMTIAZ on 6/11/17.
 */

public class ClientInteracter {
    IClientLoadedFinished listener;

    public ClientInteracter(IClientLoadedFinished listener) {
        this.listener = listener;
    }

    public void loadClientList() {
        try {
            ApiRequestUrls apis = getApiInterface(Utiles.WTP_END_POINT);
            Call<ClientModel> call = apis.getClientList();
            call.enqueue(new Callback<ClientModel>() {
                @Override
                public void onResponse(Call<ClientModel> call, final Response<ClientModel> response) {
                    Realm realm = RealmHelper.getRealmInstance();
                    try {
                        realm.executeTransaction(new Realm.Transaction() {
                            @Override
                            public void execute(Realm realm) {
                                realm.copyToRealmOrUpdate(response.body().getClient());
                            }
                        });
                    } catch (Exception e) {

                    } finally {
                        if (realm != null) {
                            realm.close();
                        }
                        listener.onNetworkSuccess();
                    }
                }

                @Override
                public void onFailure(Call<ClientModel> call, Throwable t) {
                    listener.onNetworkFailure();
                }
            });
        } catch (Exception e) {
            listener.onNetworkFailure();
        }
    }
}
