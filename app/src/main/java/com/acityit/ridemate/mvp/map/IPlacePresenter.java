package com.acityit.ridemate.mvp.map;

import android.content.Context;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by IMTIAZ on 6/11/17.
 */

public interface IPlacePresenter {
    void loadAddress(LatLng latLng, Context context);
}
