package com.acityit.ridemate.mvp.map;

import android.content.Context;

import com.acityit.ridemate.R;
import com.acityit.ridemate.models.geo_code.GeoCode;
import com.acityit.ridemate.models.place_details.PlaceDetails;
import com.acityit.ridemate.retrofit.intefaces.ApiRequestUrls;
import com.acityit.ridemate.util.Utiles;
import com.google.android.gms.maps.model.LatLng;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.acityit.ridemate.retrofit.api.ApiClient.getApiInterface;

/**
 * Created by IMTIAZ on 6/11/17.
 */

public class PlaceInteracter {

    private IPlaceLoadedFinished listener;

    public PlaceInteracter(IPlaceLoadedFinished listener) {
        this.listener = listener;
    }

    public void loadRecentCommits(final LatLng latLng, final Context context) {
        final ApiRequestUrls apis = getApiInterface(Utiles.GOOGLE_MAP_END_POINT);
        Call<GeoCode> call = apis.getGeoCode(latLng.latitude + "," + latLng.longitude);
        call.enqueue(new Callback<GeoCode>() {
            @Override
            public void onResponse(Call<GeoCode> call, Response<GeoCode> response) {
                if (response.body().getStatus().equals("OK") && response.body().getResults().size() > 0) {

                    Call<PlaceDetails> callDetails = apis.getPlaceDetailsID(response.body().getResults().get(0).getPlaceId(), context.getResources().getString(R.string.google_maps_key));
                    callDetails.enqueue(new Callback<PlaceDetails>() {
                        @Override
                        public void onResponse(Call<PlaceDetails> call, Response<PlaceDetails> response) {
                            if (response.body().getStatus().equals("OK")) {
                                com.acityit.ridemate.models.place_details.Result result = response.body().getResult();
                                listener.onNetworkSuccess(result);
                            } else {
                                listener.onNetworkFailure(latLng);
                            }
                        }

                        @Override
                        public void onFailure(Call<PlaceDetails> call, Throwable t) {
                            listener.onNetworkFailure(latLng);
                        }
                    });
                } else {
                    listener.onNetworkFailure(latLng);
                }
            }

            @Override
            public void onFailure(Call<GeoCode> call, Throwable t) {
                listener.onNetworkFailure(latLng);
            }
        });
    }
}
