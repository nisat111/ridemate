package com.acityit.ridemate.mvp.splash;

/**
 * Created by IMTIAZ on 6/11/17.
 */

public class ClientPresenter implements IClientPresenter, IClientLoadedFinished {

    private IClientView view;
    private ClientInteracter interactor;

    public ClientPresenter(IClientView view) {
        this.view = view;
        this.interactor = new ClientInteracter(this);
    }

    @Override
    public void loadClients() {
        interactor.loadClientList();
    }

    @Override
    public void onNetworkSuccess() {
        view.onClientLoadSuccess();
    }

    @Override
    public void onNetworkFailure() {
        view.onClientLoadFailure();
    }
}
