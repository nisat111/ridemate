package com.acityit.ridemate.mvp.map;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by IMTIAZ on 6/11/17.
 */

public interface IAddressView {
    void onAddressLoadedSuccess(LatLng latLng, final String name, String address, String reference, String placeId);
    void onAddressLoadedFailure(LatLng latLng);
}
