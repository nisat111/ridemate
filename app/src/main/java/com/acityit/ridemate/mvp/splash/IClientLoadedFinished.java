package com.acityit.ridemate.mvp.splash;

/**
 * Created by IMTIAZ on 6/11/17.
 */

public interface IClientLoadedFinished {
    void onNetworkSuccess();
    void onNetworkFailure();
}
