package com.acityit.ridemate.mvp.map;

import android.content.Context;

import com.acityit.ridemate.models.place_details.Result;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by IMTIAZ on 6/11/17.
 */

public class PlacePresenter implements IPlacePresenter, IPlaceLoadedFinished {

    private IAddressView view;
    private PlaceInteracter interactor;

    public PlacePresenter(IAddressView view) {
        this.view = view;
        this.interactor = new PlaceInteracter(this);
    }

    @Override
    public void loadAddress(LatLng latLng, Context context) {
        interactor.loadRecentCommits(latLng, context);
    }

    @Override
    public void onNetworkSuccess(Result result) {
        view.onAddressLoadedSuccess(new LatLng(result.getGeometry().getLocation().getLat(), result.getGeometry().getLocation().getLng()), result.getName(), result.getFormattedAddress(), result.getReference(), result.getPlaceId());
    }

    @Override
    public void onNetworkFailure(LatLng latLng) {
        view.onAddressLoadedFailure(latLng);
    }
}
