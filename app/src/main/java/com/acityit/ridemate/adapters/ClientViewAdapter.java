package com.acityit.ridemate.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.acityit.ridemate.R;
import com.acityit.ridemate.activities.TagClickListener;
import com.acityit.ridemate.retrofit.client_model.Client;
import com.acityit.ridemate.util.AnimUtiles;
import com.acityit.ridemate.util.RoundedTransformation;
import com.acityit.ridemate.util.Utiles;
import com.squareup.picasso.Picasso;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

/**
 * Created by IMTIAZ on 6/3/17.
 */

public class ClientViewAdapter extends RealmRecyclerViewAdapter<Client, ClientViewAdapter.ClientViewHolder> {

    private Context context;
    private TagClickListener tagClickListener;

    @Override
    public ClientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.client_item, parent, false);
        return new ClientViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ClientViewHolder holder, int position) {
        final Client client = getItem(position);
        holder.data = client;
        holder.name.setText(client.getName() != null ? client.getName() : "");
        holder.company.setText(client.getCompany());
        holder.country.setText(client.getCountry());
        holder.website.setVisibility(View.GONE);
        holder.playStore.setVisibility(View.GONE);
        holder.iosStore.setVisibility(View.GONE);
        if (client.getTags().size() > 0) {
            for (int i = 0; i < client.getTags().size(); i++) {
                if (client.getTags().get(i).getTag().equals("Website")) {
                    holder.website.setVisibility(View.VISIBLE);
                    holder.website.setTag(client.getTags().get(i).getUrl());
                }

                if (client.getTags().get(i).getTag().equals("Android")) {
                    holder.playStore.setVisibility(View.VISIBLE);
                    holder.playStore.setTag(client.getTags().get(i).getUrl());
                }

                if (client.getTags().get(i).getTag().equals("iOS")) {
                    holder.iosStore.setVisibility(View.VISIBLE);
                    holder.iosStore.setTag(client.getTags().get(i).getUrl());
                }
            }
        }

        if (!client.getLogo().isEmpty()) {
            holder.imgLogo.setVisibility(View.VISIBLE);
            int unit = (int) (Utiles.getDisplayMetrics(context).widthPixels * 0.15);
            Picasso.with(context)
                    .load(client.getLogo())
                    .transform(new RoundedTransformation(unit / 2, 0))
                    .resize(unit, 0)
                    .noFade()
                    .onlyScaleDown()
                    .into(holder.imgLogo);
        } else {
            holder.imgLogo.setVisibility(View.GONE);
        }
    }

    public void setTagClickListener(TagClickListener tagClickListener) {
        this.tagClickListener = tagClickListener;
    }

    class ClientViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        RelativeLayout contentLayout;
        LinearLayout tagLayout;
        TextView name, company, country, website, playStore, iosStore;
        ImageView imgLogo;
        public Client data;

        ClientViewHolder(View view) {
            super(view);
            contentLayout = (RelativeLayout) view.findViewById(R.id.contentLayout);
            tagLayout = (LinearLayout) view.findViewById(R.id.tagLayout);

            name = (TextView) view.findViewById(R.id.name);
            company = (TextView) view.findViewById(R.id.company);
            country = (TextView) view.findViewById(R.id.country);
            website = (TextView) view.findViewById(R.id.website);
            playStore = (TextView) view.findViewById(R.id.playStore);
            iosStore = (TextView) view.findViewById(R.id.iosStore);
            imgLogo = (ImageView) view.findViewById(R.id.imgLogo);

            website.setOnClickListener(this);
            playStore.setOnClickListener(this);
            iosStore.setOnClickListener(this);
            contentLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tagLayout.isShown())
                        AnimUtiles.collapse(tagLayout);
                    else
                        AnimUtiles.expand(tagLayout);
                }
            });
        }

        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.iosStore:
                    tagClickListener.openTagValue(String.valueOf(v.getTag()));
                    break;
                case R.id.playStore:
                    tagClickListener.openTagValue(String.valueOf(v.getTag()));
                    break;
                default:
                    tagClickListener.openTagValue(String.valueOf(v.getTag()));
                    break;
            }
        }
    }

    public ClientViewAdapter(@NonNull Context context, @Nullable OrderedRealmCollection<Client> data, boolean autoUpdate) {
        super(context, data, autoUpdate);
        this.context = context;
    }
}
