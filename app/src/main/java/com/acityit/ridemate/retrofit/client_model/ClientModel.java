
package com.acityit.ridemate.retrofit.client_model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.RealmList;
import io.realm.RealmObject;

public class ClientModel extends RealmObject{

    @SerializedName("client")
    @Expose
    private RealmList<Client> client;
    @SerializedName("error")
    @Expose
    private boolean error;
    @SerializedName("message")
    @Expose
    private String message;

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RealmList<Client> getClient() {
        return client;
    }

    public void setClient(RealmList<Client> client) {
        this.client = client;
    }
}
