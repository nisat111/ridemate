package com.acityit.ridemate.retrofit.intefaces;


import com.acityit.ridemate.models.geo_code.GeoCode;
import com.acityit.ridemate.models.place_details.PlaceDetails;
import com.acityit.ridemate.retrofit.client_model.ClientModel;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


/**
 * Created by IMTIAZ on 12/25/15.
 */
public interface ApiRequestUrls {

    @GET("profile/api/v2/client")
    Call<ClientModel> getClientList();

    @GET("api/geocode/json?sensor=false")
    Call<GeoCode> getGeoCode(@Query("latlng") String latlng);

    @GET("api/place/details/json?sensor=false")
    Call<PlaceDetails> getPlaceDetailsID(@Query("placeid") String placeid, @Query("key") String key);

}
