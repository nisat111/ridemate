package com.acityit.ridemate.retrofit.api;

import com.acityit.ridemate.retrofit.client_model.Client;
import com.acityit.ridemate.retrofit.client_model.Tag;
import com.acityit.ridemate.retrofit.error.ErrorHandlingExecutorCallAdapterFactory;
import com.acityit.ridemate.retrofit.error.LoggingInterceptors;
import com.acityit.ridemate.retrofit.intefaces.ApiRequestUrls;
import com.acityit.ridemate.retrofit.realm_converter.ClientRealmListConverter;
import com.acityit.ridemate.retrofit.realm_converter.TagRealmListConverter;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import io.realm.RealmList;
import io.realm.RealmObject;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by IMTIAZ on 12/25/15.
 */
public class ApiClient {
    public static final int CONNECTION_TIME_OUT = 10;
    public static final int CONNECTION_READ_TIME_OUT = 30;

    public static ApiRequestUrls getApiInterface(String api_url) {
        Gson gson = new GsonBuilder()
                .setExclusionStrategies(new ExclusionStrategy() {
                    @Override
                    public boolean shouldSkipField(FieldAttributes f) {
                        return f.getDeclaringClass().equals(RealmObject.class);
                    }

                    @Override
                    public boolean shouldSkipClass(Class<?> clazz) {
                        return false;
                    }
                })
                .registerTypeAdapter(new TypeToken<RealmList<Client>>() {}.getType(), new ClientRealmListConverter())
                .registerTypeAdapter(new TypeToken<RealmList<Tag>>() {}.getType(), new TagRealmListConverter())
                .serializeNulls()
                .setLenient() // support malforms character(s)
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(api_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(new ErrorHandlingExecutorCallAdapterFactory(new ErrorHandlingExecutorCallAdapterFactory.MainThreadExecutor()))
                .client(getClient())
                .build();

        return retrofit.create(ApiRequestUrls.class);
    }

    public static class LogJsonInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();

            Response response = chain.proceed(request);
            String rawJson = response.body().string();
            return response.newBuilder()
                    .body(ResponseBody.create(response.body().contentType(), rawJson)).build();
        }
    }

    private static OkHttpClient getClient() {
        return new OkHttpClient.Builder()
                .connectTimeout(CONNECTION_TIME_OUT, TimeUnit.SECONDS)
                .writeTimeout(CONNECTION_TIME_OUT, TimeUnit.SECONDS)
                .readTimeout(CONNECTION_READ_TIME_OUT, TimeUnit.SECONDS)
//                .addInterceptor(new LogJsonInterceptor())
//                .addInterceptor(getLogging())
//                .addInterceptor(new LoggingInterceptor())
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request().newBuilder()
                                .addHeader("authorization", "32DFCFD@#&DSFDSFSDF!L@?hh7@32DF")
                                .build();
                        return chain.proceed(request);
                    }
                })
                .build();
    }


    private static HttpLoggingInterceptor getLogging() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }

    private static final Logger logger = Logger.getLogger(LoggingInterceptors.class.getName());

    private static class LoggingInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            long t1 = System.nanoTime();
            Request request = chain.request();
            logger.info(String.format("Sending request %s on %s%n%s",
                    request.url(), chain.connection(), request.headers()));
            Response response = chain.proceed(request);

            long t2 = System.nanoTime();
            logger.info(String.format("Received response for %s in %.1fms%n%s",
                    request.url(), (t2 - t1) / 1e6d, response.headers()));
            return response;
        }
    }

}
