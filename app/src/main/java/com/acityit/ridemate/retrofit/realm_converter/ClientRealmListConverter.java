package com.acityit.ridemate.retrofit.realm_converter;

import com.acityit.ridemate.retrofit.client_model.Client;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import java.lang.reflect.Type;

import io.realm.RealmList;

/**
 * Created by IMTIAZ on 6/2/17.
 */

public class ClientRealmListConverter implements JsonSerializer<RealmList<Client>>,
        JsonDeserializer<RealmList<Client>> {

    @Override
    public JsonElement serialize(RealmList<Client> src, Type typeOfSrc,
                                 JsonSerializationContext context) {
        JsonArray ja = new JsonArray();
        for (Client tag : src) {
            ja.add(context.serialize(tag));
        }
        return ja;
    }

    @Override
    public RealmList<Client> deserialize(JsonElement json, Type typeOfT,
                                      JsonDeserializationContext context)
            throws JsonParseException {
        RealmList<Client> tags = new RealmList<>();
        JsonArray ja = json.getAsJsonArray();
        for (JsonElement je : ja) {
            tags.add((Client) context.deserialize(je, Client.class));
        }
        return tags;
    }

}