package com.acityit.ridemate.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.acityit.ridemate.R;
import com.acityit.ridemate.mvp.map.view.MapActivity;
import com.acityit.ridemate.util.Utiles;

public class MainActivity extends AppCompatActivity {
    ImageView imgVan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        imgVan = (ImageView) findViewById(R.id.imgVan);

        Utiles.GPSEnableAlert(MainActivity.this, getResources().getString(R.string.location_details));

        findViewById(R.id.btnMap).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveVan(new Intent(MainActivity.this, MapActivity.class));
            }
        });

        findViewById(R.id.btnClient).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveVan(new Intent(MainActivity.this, ClientActivity.class));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        imgVan.setVisibility(View.VISIBLE);
    }

    private void moveVan(final Intent intent) {
        Animation localAnimation = AnimationUtils.loadAnimation(this, R.anim.move);
        localAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                imgVan.setVisibility(View.GONE);
                startActivity(intent);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        imgVan.startAnimation(localAnimation);
    }
}
