package com.acityit.ridemate.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.acityit.ridemate.R;
import com.acityit.ridemate.adapters.ClientViewAdapter;
import com.acityit.ridemate.retrofit.client_model.Client;
import com.acityit.ridemate.util.RealmHelper;
import com.acityit.ridemate.util.RecyclerViewEmptySupport;

import io.realm.Realm;

/**
 * Created by IMTIAZ on 6/4/17.
 */

public class ClientActivity extends BaseActivity implements TagClickListener {
    private Realm realm;
    private RecyclerViewEmptySupport recyclerView;
    private ClientViewAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.client_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        realm = RealmHelper.getRealmInstance();
        adapter = new ClientViewAdapter(this, realm.where(Client.class).findAll(), true);
        adapter.setTagClickListener(this);
        recyclerView = (RecyclerViewEmptySupport) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setEmptyView(findViewById(R.id.empty_view));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        recyclerView.setAdapter(null);
        if (realm != null) realm.close();
    }

    public void openUrl(String url) {
        if (!url.startsWith("https://") && !url.startsWith("http://")){
            url = "http://" + url;
        }
        Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(i);
    }

    @Override
    public void openTagValue(String url) {
        openUrl(url);
    }
}
