package com.acityit.anim.listener;

import com.acityit.anim.ExpectAnim;

/**
 * Created by florentchampigny on 21/02/2017.
 */

public interface AnimationEndListener {
    void onAnimationEnd(ExpectAnim expectAnim);
}
